AvenueCode DevOps Challenge
======

Simple Ansible configuration management.

Requirements
======
*   Environment:
    -   Ansible
    
Usage on Local Hosts
======
Reminder: All commands must be executed within the root project folder.

1. Deploy tomcat server (if doesn't exist): `sudo ansible-playbook -i "localhost," -c local devops-challenge/tomcat-deploy.yml`
2. Deploy .war file to tomcat server: `sudo ansible-playbook -i "localhost," -c local devops-challenge/app-deploy.yml`

Usage on Remote Hosts
======
Reminder: All commands must be executed within the root project folder.

1. Deploy tomcat server (if doesn't exist): `sudo ansible-playbook -i {HOST_FILE} devops-challenge/tomcat-deploy.yml`
2. Deploy .war file to tomcat server: `sudo ansible-playbook -i {HOST_FILE} devops-challenge/app-deploy.yml`
        
HOST_FILE: file containing the hosts that ansible should connect and execute the playbooks.